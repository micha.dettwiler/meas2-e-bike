% W�rmetransport mds-Modellierungsprojekt
% Autor: Ahmed Mohammad und Klemens Arnold 20. Januar 2017

% Parameter zur�cksetzen

clc, clf, clear, close all


% Konstanten

rho_cu = 8930;   % Dichte Kupfer in kg/m^3
c = 394;         % Spez. Waermekapazit�t Kupfer in J/(kg*K)
lambda = 384;    % W�rmeleitf�higkeit Kupfer in W/(m*K)
alpha = 40;      % W�rmeuebergangskapazit�t zw. Kupfer und Luft in W/(m^2*K)
s = 0.02;        % L�nge eines Segments in m
b = 0.005;       % Breite eines Segments in m
h = 0.06;        % H�he eines Segments in m
P_el = 5;        % Heizleistung in W
T_U = 20;        % Umgebungstemperatur in �C


% Berechnungen aus Konstanten

A_WT = b*h;              %Ber�hrungsfl�che zw. Segmente
A_WU1 = 2*s*(h+b)+b*h;   %Oberfl�che W�rme�bergang 1. und 5. Segement
A_WU2 = 2*s*(h+b);       %Oberfl�che der Segemente 2-4
m = (s*b*h)*rho_cu;      %Masse eines Segments


% Simulink-Modell laden

mdl = 'WaermetransportToWorkspace';
load_system(mdl);


% Simulink-Modell Konfiguration:

% Startzeit: Start time
set_param(mdl,'StartTime','0.0');
% Endzeit: Stop time
set_param(mdl,'StopTime','1400.0');
% Verfahren: Solver
% (ode1: Euler-Cauchy, ode2: Heun, ode4: Runge-Kutta klassisch)
set_param(mdl,'Solver','ode4');
% Schrittweite: FixedStep
set_param(mdl,'FixedStep','1');
% Ausgabe zu MATLAB konfigurieren SaveFormat=StructureWithTime
set_param(strcat(mdl,'/To Workspace'),'SaveFormat','StructureWithTime');


% Simulink-Modell einlesen

tic;
sim(mdl);
toc;


% Extraktion der Werte

t = T.time;                         % Zeit in s
T = T.signals.values;               % Temp. Segment 1-5 in �C
Q_WT = Q_WT.signals.values;         % W�rmetransport Segment 1-4 in J
Q_WU = Q_WU.signals.values;         % W�rme�bergang Segment 1-5 in J


% Darstellung der numerischen Simulink L�sung

figure ('Name','W�rmetransport mds-Modellierungsprojekt','NumberTitle','off');

subplot(3,1,1);
plot(t,T);
grid on; xlabel('t [s]'); ylabel('T [�C]');
legend('T_1','T_2','T_3','T_4','T_5');
legend('Location','northeast');
title('Temperaturverlauf in den Segmenten');

subplot(3,1,2);
plot(t,Q_WT);
grid on; xlabel('t [s]'); ylabel('Q [J]');
legend('Q_W_T_1','Q_W_T_2','Q_W_T_3','Q_W_T_4');
legend('Location','northeast');
title('W�rmetransport zwischen den Segmenten');

subplot(3,1,3);
plot(t,Q_WU);
grid on; xlabel('t [s]'); ylabel('Q [J]');
legend('Q_W_U_1','Q_W_U_2','Q_W_U_3','Q_W_U_4','Q_W_U_5');
legend('Location','northeast');
title('W�rme�bergang in den Segmenten');